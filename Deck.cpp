#include "Deck.h"
const int NUMBER_OF_FEATURES = 3;
Deck::Deck()
{
	m_cardIndex = 0;
	for (int i = 0; i < NUMBER_OF_FEATURES; i++)
		for (int j = 0; j < NUMBER_OF_FEATURES; j++)
			for (int k = 0; k < NUMBER_OF_FEATURES; k++)
				for (int l = 0; l < NUMBER_OF_FEATURES; l++)
					m_cardsList[m_cardIndex++] = Card(static_cast<Card::Number>(i), static_cast<Card::Symbol>(j), static_cast<Card::Shading>(k), static_cast<Card::Color>(l));
	m_cardIndex = 0;
}

void Deck::shuffleDeck()
{
	m_cardIndex = 0;
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, DECK_SIZE - 1);

	for (auto i = 0; i<DECK_SIZE; i++)
	{
		int swapIndex = dis(gen);
		Card tempCard = m_cardsList[i];
		m_cardsList[i] = m_cardsList[swapIndex];
		m_cardsList[swapIndex] = tempCard;
	}
}

void Deck::printDeck() const
{
	for (int i = 0; i < DECK_SIZE; i++)
	{
		m_cardsList[i].printCard();
		std::cout << std::endl;
	}
}

Card Deck::drawCard()
{
	if (m_cardIndex < DECK_SIZE)
		return m_cardsList[m_cardIndex++];
	return Card(Card::Number::NUMBER_END, Card::Symbol::SYMBOL_END, Card::Shading::SHADING_END, Card::Color::COLOR_END);
}
