#include "GameBoard.h"

GameBoard::GameBoard(std::array<Card, MAXIMUM_CARD_NUMBER> initialCards)
{
	m_actualDeckSize = 0;
	std::cout << "Cartile initiale: " << std::endl;
	for (int i = 0; i < MAXIMUM_CARD_NUMBER; i++)
	{
		pushCard(initialCards[i]);
		m_cardList[i].printCard();
		std::cout << std::endl;
	}

}

void GameBoard::pushCard(Card card)
{
	m_cardList[m_actualDeckSize++] = card;
}

bool GameBoard::F1()
{
	bool found = false;
	int i, j, k;
	for (i = 0; i < MAXIMUM_CARD_NUMBER - 2 && !found; i++)
		for (j = i + 1; j < MAXIMUM_CARD_NUMBER - 1 && !found; j++)
			for (k = j + 1; k < MAXIMUM_CARD_NUMBER && !found; k++)
				if (F2(i, j, k))
					found = true;
	if (found)
	{
		std::cout << "Au fost eliminate cartile: " << std::endl;
		m_cardList[i].printCard();
		std::cout << std::endl;

		m_cardList[j].printCard();
		std::cout << std::endl;

		m_cardList[k].printCard();
		std::cout << std::endl;

		removeCards(i, j, k);

	}
	return found;
}



bool GameBoard::F2(int posI, int posJ, int posK)
{
	//check if the features are the same
	bool checkNumber = m_cardList[posI].getNumber() == m_cardList[posJ].getNumber() && m_cardList[posJ].getNumber() == m_cardList[posK].getNumber();
	bool checkColor = m_cardList[posI].getColor() == m_cardList[posJ].getColor() && m_cardList[posJ].getColor() == m_cardList[posK].getColor();
	bool checkSymbol = m_cardList[posI].getSymbol() == m_cardList[posJ].getSymbol() && m_cardList[posJ].getSymbol() == m_cardList[posK].getSymbol();
	bool checkShading = m_cardList[posI].getShading() == m_cardList[posJ].getShading() && m_cardList[posJ].getShading() == m_cardList[posK].getShading();

	if (checkNumber || checkSymbol || checkShading || checkColor)
		return true;

	//check if the features are different
	checkNumber = (m_cardList[posI].getNumber() != m_cardList[posJ].getNumber()) &&
		(m_cardList[posI].getNumber() != m_cardList[posK].getNumber()) &&
		(m_cardList[posJ].getNumber() != m_cardList[posK].getNumber());

	checkColor = (m_cardList[posI].getColor() != m_cardList[posJ].getColor()) &&
		(m_cardList[posI].getColor() != m_cardList[posK].getColor()) &&
		(m_cardList[posJ].getColor() != m_cardList[posK].getColor());

	checkSymbol = (m_cardList[posI].getSymbol() != m_cardList[posJ].getSymbol()) &&
		(m_cardList[posI].getSymbol() != m_cardList[posK].getSymbol()) &&
		(m_cardList[posJ].getSymbol() != m_cardList[posK].getSymbol());

	checkShading = (m_cardList[posI].getShading() != m_cardList[posJ].getShading()) &&
		(m_cardList[posI].getShading() != m_cardList[posK].getShading()) &&
		(m_cardList[posJ].getShading() != m_cardList[posK].getShading());

	if (checkNumber || checkSymbol || checkShading || checkColor)
		return true;
	return false;
}

void GameBoard::printCards()
{
	std::cout << "Noile carti de pe board sunt: "<<std::endl;
		for (int i = 0; i < m_actualDeckSize; i++)
		{
			m_cardList[i].printCard();
			std::cout << std::endl;
		}
}

void GameBoard::removeCards(int posI, int posJ, int posK)
{
	for (int i = posK; i < m_actualDeckSize - 1; i++)
		m_cardList[i] = m_cardList[i + 1];
	m_actualDeckSize--;

	for (int i = posJ; i < m_actualDeckSize - 1; i++)
		m_cardList[i] = m_cardList[i + 1];
	m_actualDeckSize--;

	for (int i = posI; i < m_actualDeckSize - 1; i++)
		m_cardList[i] = m_cardList[i + 1];
	m_actualDeckSize--;
}
