#include "Dealer.h"

Dealer::Dealer()
{

	m_deck.shuffleDeck();
}

Card Dealer::drawCard()
{
	return m_deck.drawCard();
}
