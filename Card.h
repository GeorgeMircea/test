#pragma once
#include<iostream>
class Card
{
public:
	enum Number { ONE, TWO, THREE, NUMBER_END };
	enum Symbol { DIAMOND, SQUIGGLE, OVAL, SYMBOL_END };
	enum Shading { SOLID, STRIPED, OPEN, SHADING_END };
	enum Color { RED, GREEN, BLUE, COLOR_END };

	Card();
	Card(Number, Symbol, Shading, Color);

	Card::Number getNumber() const;
	Card::Symbol getSymbol() const;
	Card::Shading getShading() const;
	Card::Color getColor() const;

	void printCard() const;
private:
	Number m_number;
	Symbol m_symbol;
	Shading m_shading;
	Color m_color;


};