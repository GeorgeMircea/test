#pragma once
#include "Deck.h"
const int MAXIMUM_CARD_NUMBER = 12;
class GameBoard
{
public:
	GameBoard(std::array<Card, MAXIMUM_CARD_NUMBER>);
	void pushCard(Card);

	bool F1();
	bool F2(int, int, int);

	void printCards();

private:
	void removeCards(int, int, int);
	int m_actualDeckSize;
	std::array<Card, MAXIMUM_CARD_NUMBER> m_cardList;
	std::string m_gameBoardName="Gerhart";
};