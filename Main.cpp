#include"GameBoard.h"
#include"Dealer.h"

const int DECK_ON_BOARD_SIZE = 12;
int main()
{
	Dealer dealer;

	std::array<Card, DECK_ON_BOARD_SIZE> cardList;
	for (int i = 0; i < DECK_ON_BOARD_SIZE; i++)
		cardList[i] = dealer.drawCard();

	GameBoard gameBoard(cardList);
	std::cout << std::endl;

	Card drewCard = dealer.drawCard(); //sorry for my english!
	while (drewCard.getColor() != Card::Color::COLOR_END)
	{
		if (gameBoard.F1())
		{
			gameBoard.pushCard(drewCard);

			drewCard = dealer.drawCard();
			gameBoard.pushCard(drewCard);

			drewCard = dealer.drawCard();
			gameBoard.pushCard(drewCard);

			drewCard = dealer.drawCard();
			std::cout << std::endl;
		}

		gameBoard.printCards();
		std::cout << std::endl;
	}

	return 0;
}