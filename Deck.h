#pragma once
#include"Card.h"
#include<array>
#include<string>
#include<random>
const int DECK_SIZE = 81;
class Deck
{
public:
	Deck();
	void shuffleDeck();
	void printDeck() const;

	Card drawCard();
private:
	int m_cardIndex;
	std::array<Card, DECK_SIZE> m_cardsList;
	const std::string m_deckName="Francois";
};