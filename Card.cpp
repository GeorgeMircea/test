#include "Card.h"

Card::Card()
{
}

Card::Card(Number number, Symbol symbol, Shading shading, Color color):m_number(number), m_symbol(symbol), m_shading(shading), m_color(color)
{
}

Card::Number Card::getNumber() const
{
	return m_number;
}

Card::Symbol Card::getSymbol() const
{
	return m_symbol;
}

Card::Shading Card::getShading() const
{
	return m_shading;
}

Card::Color Card::getColor() const
{
	return m_color;
}

void Card::printCard() const
{
	switch (m_number)
	{
	case Card::ONE:
		std::cout << "ONE ";
		break;
	case Card::TWO:
		std::cout << "TWO ";
		break;
	case Card::THREE:
		std::cout << "THREE ";
		break;
	case Card::NUMBER_END:
		break;
	default:
		break;
	}

	switch (m_symbol)
	{
	case Card::DIAMOND:
		std::cout << "DIAMOND ";
		break;
	case Card::SQUIGGLE:
		std::cout << "SQUIGGLE ";
		break;
	case Card::OVAL:
		std::cout << "OVAL ";
		break;
	case Card::SYMBOL_END:
		break;
	default:
		break;
	}

	switch (m_shading)
	{
	case Card::SOLID:
		std::cout << "SOLID ";
		break;
	case Card::STRIPED:
		std::cout << "STRIPED ";
		break;
	case Card::OPEN:
		std::cout << "OPEN ";
		break;
	case Card::SHADING_END:
		break;
	default:
		break;
	}

	switch (m_color)
	{
	case Card::RED:
		std::cout << "RED ";
		break;
	case Card::GREEN:
		std::cout << "GREEN ";
		break;
	case Card::BLUE:
		std::cout << "BLUE ";
		break;
	case Card::COLOR_END:
		break;
	default:
		break;
	}
}
