#pragma once
#include"GameBoard.h"
class Dealer
{
public:
	Dealer();
	Card drawCard();
private:
	const std::string m_dealerName = "Johnny";
	Deck m_deck;
};